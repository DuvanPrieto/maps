import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser'
import { Task } from './../interfaces/task';
import { Alert } from './../interfaces/alert';
@Injectable({
  providedIn: 'root'
})
export class TaskService {

  testUrl:any
  constructor(
   private http: HttpClient,
   private dom:DomSanitizer
  ) {
    this.testUrl = this.dom.bypassSecurityTrustResourceUrl("https://consultas.axuretechnologies.com:8443/cxf/ws.axure/Alertas/Axure");
    
   }
  
  getAllTasks() {
    const path = 'https://consultas.axuretechnologies.com:8443/cxf/axure_tsw/estado/axure';
    return this.http.get<Task[]>(path);
  }
  getAllAlerts() {
    const path = 'https://consultas.axuretechnologies.com:8443/cxf/ws.axure/Alertas/Axure';
    return this.http.get<Alert[]>(path);
  }
  getTask(id: string) {
    const path = `https://consultas.axuretechnologies.com:8443/cxf/axure_tsw/estado/${id}`;
    return this.http.get<Task[]>(path);
  }
}
