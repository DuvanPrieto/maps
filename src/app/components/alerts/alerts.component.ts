import { Component, OnInit } from '@angular/core';
import { TaskService } from '../../services/task.service';
import { AuthService } from '../../services/auth.service';
import * as $ from 'jquery';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from "angularfire2/firestore";
import * as firebase from 'firebase';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit {

  lat: number = 7.0327778;
  lng: number = -73.9147222;
  
  arreglo = [];
  constructor(private taskService:TaskService,private authService: AuthService) { 

    this.taskService.getAllAlerts()
    .subscribe(infoTask => {
      this.arreglo = infoTask;
      
      // console.log(this.arreglo);
      if(this.arreglo.length == 0)
      {
        console.log("Error de conexión");
      }else
      {
        $(".audio")[0].play();
      }


    });
    
    setInterval(() => {
      this.taskService.getAllAlerts()
      .subscribe(infoTask => {
        this.arreglo = infoTask;
         console.log(this.arreglo);
        if(this.arreglo.length == 0)
        {
          console.log("Error de conexión");
        }else
        {
          $(".audio")[0].play();
        }



      });
        }, 20000)      


  }
  ngOnInit() {
    
    $(".NavAdminBar").click(function(){
      if($(".contenedoritemsBarAdmin").hasClass('ActivarBarAdmin'))
      {
        $(".contenedoritemsBarAdmin").removeClass('ActivarBarAdmin')
      }
      else
      {
        $(".contenedoritemsBarAdmin").addClass('ActivarBarAdmin')
      }
});
    
    var openBtn = true;
      $(".itemNavBarAdmin3").click(function(){
          if(openBtn!)
          {
            $(".contenedorConvenciones").addClass('openBtn');  
          }
          else
          {
           
            $(".contenedorConvenciones").removeClass('openBtn');
          }
          openBtn = !openBtn;
      })
    // $(".inferiorconvenciones").click(function(){
      

    // });
  }
  OnLogout()
  { 
    
    this.authService.LogOutUser();
    location.href ="/";
  }
  

}

