import { Component, OnInit } from '@angular/core';
import {TaskService} from '../../../services/task.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-maps-admin',
  templateUrl: './maps-admin.component.html',
  styleUrls: ['./maps-admin.component.css']
})
export class MapsAdminComponent implements OnInit {

  lat: number = 7.0327778;
  lng: number = -73.9147222;
  
  arreglo = [];
  arreglo2 = [];
  constructor( private taskService:TaskService) {

    this.taskService.getAllTasks()
    .subscribe(infoTask => {
      this.arreglo = infoTask;
      // console.log(this.arreglo);
      if(this.arreglo.length == 0)
      {
        alert("Error de conexión");
      }

    });
    
    setInterval(() => {
      this.taskService.getAllTasks()
      .subscribe(infoTask => {
        this.arreglo = infoTask;
        // console.log(this.arreglo);
        if(this.arreglo.length == 0 )
        {
          alert("Error de conexión");
        }
      });
        }, 8000)
        
        // SERVICIO ALERTAS
        this.taskService.getAllAlerts()
        .subscribe(infoTask => {
          this.arreglo2 = infoTask;
          
          // console.log(this.arreglo);
          if(this.arreglo2.length == 0)
          {
            console.log("Error de conexión");
          }else
          {
            $(".audio")[0].play();
          }
    
    
        });
        
        setInterval(() => {
          this.taskService.getAllAlerts()
          .subscribe(infoTask => {
            this.arreglo2 = infoTask;
             console.log(this.arreglo2);
            if(this.arreglo2.length == 0)
            {
              console.log("Error de conexión");
            }else
            {
              $(".audio")[0].play();
            }
    
    
    
          });
            }, 20000)
      
   }

  ngOnInit() {

  }

}




